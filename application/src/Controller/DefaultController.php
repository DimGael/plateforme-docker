<?php

    namespace App\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use App\Service\ArticleService;
    use Symfony\Component\Routing\Annotation\Route;


    class DefaultController extends AbstractController
    {
        /** @var ArticleService  */
        private $articleService;

        public function __construct(ArticleService $articleService)
        {
            $this->articleService = $articleService;
        }

        /**
         * @Route("/", name="home")
         */
        public function index()
        {
            $articles = $this->articleService->findAllCleanedArticle();
            return $this->render('default/home.html.twig',[
                'articleList' => $articles
            ]);
         }
    }