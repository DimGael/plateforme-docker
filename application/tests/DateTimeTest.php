<?php
/**
 * Created by PhpStorm.
 * User: gael
 * Date: 18/10/18
 * Time: 11:36
 */

use PHPUnit\Framework\TestCase;

class DateTimeTest extends TestCase
{
    /**
     * @test
     */
    public function shouldFormatDate() {
        $dateTime = new DateTime('2016-09-01');
        $this->assertEquals('01/09/2016', $dateTime->format('d/m/Y'));
    }
    /**
     * @test
     */
    public function shouldAddInterval() {
        $dateTime = new DateTime('2016-09-01');
        try {
            $interval = new DateInterval('P2D');
        } catch (Exception $e) {

        }
        $dateTime->add($interval);
        $this->assertEquals('03/09/2016', $dateTime->format('d/m/Y'));
    }

}